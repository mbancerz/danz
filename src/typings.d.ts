/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

interface SearchResult {
  vote_count: number; // 18,
  id: number; // 36380,
  video: boolean; // false,
  vote_average: number; // 7.5,
  title: string; // Chłopaki nie płaczą,
  popularity: number; // 1.081914,
  poster_path: string; // /ktAJqpPjYOnVWWXRRXle6oODMhT.jpg,
  original_language: string; // pl,
  original_title: string; // Chlopaki nie placza,
  genre_ids: number[]; // [35],
  backdrop_path: string; // /3UWqqTSAgleBk3vG9RLZUlJAUEc.jpg,
  adult: boolean; // false,
  overview: string; // Kuba Brenner (Maciej Stuhr) jest młodym, dobrze zapowiadającym się skrzypkiem
  release_date: string; // 2000-02-25
}

interface SearchResponse {
  page: number;
  total_results: number;
  total_pages: number;
  results: SearchResult[];
}

interface Genre {
  id: number;
  name: string;
}

interface ProductionCompany {
  name: string;
  id: number;
}

interface ProductionCountry {
  iso_3166_1: string;
  name: string;
}

interface SpokenLanguages {
  iso_639_1: string;
  name: string;
}

interface MovieResponse {
  adult: boolean; // false,
  backdrop_path: string; // /3UWqqTSAgleBk3vG9RLZUlJAUEc.jpg,
  belongs_to_collection: any; // null,
  budget: number; // 0,
  genres: Genre[]; // [{id: 35, name: Comedy}],
  homepage: string; // ,
  id: number; // 36380,
  imdb_id: string; // tt0238119,
  original_language: string; // pl,
  original_title: string; // Chlopaki nie placza,
  overview: string; // One of the best Polish comedies of the late 1990's, \Boys Don't Cry\ is a satirical look at the gangsters of Poland and some teens who accidentally get involved with them.,
  popularity: string; // 0.081914,
  poster_path: string; // /ktAJqpPjYOnVWWXRRXle6oODMhT.jpg,
  production_companies: ProductionCompany[];
  production_countries: ProductionCountry[];
  release_date: string; // 2000-02-25,
  revenue: number; // 0,
  runtime: number; // 90,
  spoken_languages: SpokenLanguages;
  status: string; // Released,
  tagline: string; // ,
  title: string; // Boys Don't Cry,
  video: boolean; // false,
  vote_average: number; // 7.5,
  vote_count: number; // 18
}