import { ApiService } from '../api.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-searcher',
    templateUrl: './searcher.component.html',
    styleUrls: ['./searcher.component.css']
})
export class SearcherComponent implements OnInit {

    public searchForm: FormGroup;
    @Output() public searchResponse: SearchResponse;
    @Output() public currentPage = 1;

    constructor(private fb: FormBuilder, private api: ApiService) { }

    ngOnInit() {
        this.searchForm = this.fb.group({
            query: new FormControl('', Validators.required)
        });
    }

    public onPageChanged(event: any): void {
        this.currentPage = event.page;
        this.doSearch();
    }

    public doSearch() {
        const query: string = this.searchForm.get('query').value;
        if (query.length > 0) {
            this.api.searchMovie(encodeURI(query), this.currentPage).then((res) => {
                this.searchResponse = res;
            }).catch((error) => {
                console.error('Search movie failed', error);
            });
        }
    }

}
