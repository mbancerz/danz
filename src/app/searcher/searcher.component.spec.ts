import { JsonpModule } from '@angular/http';
import { ApiService } from '../api.service';
import { RouterTestingModule } from '@angular/router/testing';
import { TruncatePipe } from '../truncate.pipe';
import { DisplayerComponent } from '../displayer/displayer.component';
import { PaginationModule } from 'ngx-bootstrap/ng2-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearcherComponent } from './searcher.component';

describe('SearcherComponent', () => {
    let component: SearcherComponent;
    let fixture: ComponentFixture<SearcherComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SearcherComponent, DisplayerComponent, TruncatePipe],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                PaginationModule.forRoot(),
                RouterTestingModule,
                JsonpModule
            ],
            providers: [
                ApiService
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearcherComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('Should call api on change page', () => {
        const spyOnDoSearch = spyOn(component, 'doSearch').and.callFake(() => { });
        const expectedPage = 5;
        component.onPageChanged({ page: 5 });
        fixture.detectChanges();
        expect(component.currentPage).toBe(expectedPage);
        expect(spyOnDoSearch).toHaveBeenCalled();
    });

    it('Should call api on search request', (done) => {
        const api: ApiService = TestBed.get(ApiService);
        const spyOnApiSearchMovie = spyOn(api, 'searchMovie').and.returnValue(Promise.resolve('xxx'));
        component.searchForm.get('query').setValue('bbb');
        component.currentPage = 5;
        component.doSearch();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            expect(spyOnApiSearchMovie).toHaveBeenCalledWith('bbb', 5);
            expect(component.searchResponse).toBe(<any>'xxx');
            done();
        });
    });
});
