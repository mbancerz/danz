import { SearcherComponent } from './searcher/searcher.component';
import { DetailsComponent } from './details/details.component';
import { AppComponent } from './app.component';
import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const appRoutes: Route[] = [
    {
        path: '',
        component: SearcherComponent
    },
    {
        path: 'movie/:id',
        component: DetailsComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRouting {}
