import { HttpModule, JsonpModule } from '@angular/http';
import { AppModule } from './app.module';
import { TestBed, inject } from '@angular/core/testing';

import { ApiService } from './api.service';

describe('ApiService', () => {
    let service: ApiService;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ApiService],
            imports: [JsonpModule, HttpModule]
        });

        service = TestBed.get(ApiService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call valid url for getMovie', (done) => {
        const spyOnRequest = spyOn(service, <any>'request').and.returnValue(Promise.resolve(null));
        const expectedUrl = `https://api.themoviedb.org/3/movie/1?api_key=618913a5ce370160903d0cc6f927c510`;
        service.getMovie(1).then((res) => {
            expect(spyOnRequest).toHaveBeenCalledWith(expectedUrl);
            expect(res).toBe(null);
            done();
        });
    });

    it('should call valid url for searchMovie', (done) => {
        const spyOnRequest = spyOn(service, <any>'request').and.returnValue(Promise.resolve(null));
        const expectedUrl = `https://api.themoviedb.org/3/search/movie/?api_key=618913a5ce370160903d0cc6f927c510&query=aaa&page=1`;
        service.searchMovie('aaa').then((res) => {
            expect(spyOnRequest).toHaveBeenCalledWith(expectedUrl);
            expect(res).toBe(null);
            done();
        });
    });
});
