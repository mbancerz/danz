import { Http, Jsonp } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {

    private API_KEY = '618913a5ce370160903d0cc6f927c510';
    private HOST_URL = 'https://api.themoviedb.org/3';

    constructor(private jsonp: Jsonp) {}

    /**
     * Call API service by JSONP
     * @private
     */
    private request = <T>(url: string) => new Promise<T>((onSuccess, onError) => {
        this.jsonp.get(url + '&callback=JSONP_CALLBACK').subscribe((res) => {
            onSuccess(res.json() as T);
        }, (error) => {
            console.error({error});
            onError(error);
        });
    })

    /**
     * Search for movies by $query
     */
    public searchMovie = (query: string, page = 1) => {
        // tslint:disable-next-line:max-line-length
        const url = `${this.HOST_URL}/search/movie/?api_key=${this.API_KEY}&query=${query}&page=${page}`;
        return this.request<SearchResponse>(url);
    }

    /**
     * Get movie data by $id
     */
    public getMovie = (id: number) => {
        const url = `${this.HOST_URL}/movie/${id}?api_key=${this.API_KEY}`;
        return this.request<MovieResponse>(url);
    }
}
