import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

    public movie: MovieResponse;
    public isReady = false;

    constructor(private route: ActivatedRoute, private api: ApiService) { }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            const id = +params['id'];
            this.load(id);
        });
    }

    private load = (id: number) => {
        this.api.getMovie(id).then((movie) => {
            this.movie = movie;
            this.isReady = true;
        }).catch((error) => {
            console.error(error);
        });
    }

}
