import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-displayer',
    templateUrl: './displayer.component.html',
    styleUrls: ['./displayer.component.scss']
})
export class DisplayerComponent implements OnInit {

    @Input() results: SearchResponse;
    @Input() page: number;

    constructor() { }

    ngOnInit() {
    }

}
